/*
 * Klasa ma demonstrować działanie wykonywania działań w programowaniu
 * WPROWADZA POJĘCIE ZMIENNEJ!
 * Zmienna to miejsce zapisu danych zmieniających się (np. wprowadzanych przez użytkownika lub
 * ulegające zmianie w trakcie działania programu) w pamięci operacyjnej (RAM). W przypadku
 * języka Java każda zmienna ma swój typ oraz romiar (czyli ile komórek pamięci zajmuje)
 */
public class Dzialania {
	
	public static void main(String[] args) {
		/*w Java istniają następujące typy zmiennych (liczbowe podstawowe, najczęściej używane):
		 * - int - liczba całkowita 32 bitowa (ok. 4 miliardy, połowa ujemna)
		 * - float - liczba zmiennoprzecinkowa (pływający przecinek), nazywana zmienną pojedynczej
		 * precyzji (32 bity)
		 * - double - zmiennoprzecinkowe podwójnej precyzji (64 bity)
		 * -boolean - typ prawda fałsz, zajmuje 1 bajt (wartość 0 to fałsz i każda
		 * dodatania to prawda)
		 * 
		 * LICZBOWE ROZSZERZONE (rzadziej używane, precyzujące):
		 * - byte - zmienna 8bit (1 bajt); przechowuje dowolną zawartość (liczbową)
		 * - short - liczba całkowita 16 bitowa 
		 * - long - liczba całkowita 64 bita
		 * 
		 * Zmienne znakowe podstawowe:
		 * - char - wartość przechowuje JEDEN znak (dowolny ASCII, podstawowy bądz rozszerzony)
		 * - byte - jako typ nieokreślony może istnieć jako wartość znakowa
		 * 
		 * Zmienna znakowe SPECJALNE:
		 * - String - zmienna pozwala na przechowywanie serii znaków drukowanych (stąd nazwa string ->
		 * łańcuch); to ona jest najczęściej stosowana do znaków (jest to tzw. zmienna złożona.
		 * 
		 * Zmienne tworzy się poprzez podanie ich typu oraz nazwy. Nazwa może być dowolna, jednak
		 * trzeba pamiętać, że:
		 * - nie może zaczynać się od liczby
		 * - nie może zawierać znaków specjalnych (szczególnie używanych w języku programowania)
		 * - nie może przyjmować nazw zastrzeżonych przez język (sprawdzić jakie to)
		 * 
		 * Przykłady zmiennych:
		 */
		int liczba1;
		float przecinek;
		char znak;
		
		//i ich wykorzystanie - tutaj przypisanie wartości w programie
		
		liczba1 = 145;
		przecinek = 0.098f; //<- musi być litera f oznaczająca wartość jako float
		znak = 'C';
		
		//przykład użycia zmiennych  - wyświetlenie na konsoli:
		
		//plus w poniższym wyświetleniu mowi kompilatorowi Java, że ma złączyć do ciągu zakowego
		//napisanego po lewej zawartość zmiennej liczba1. 
		System.out.println("Wartość zmiennej liczba1 wynosi " + liczba1);
		
		System.out.println("Wartość liczby zmiennoprzecinkowej wynosi " + przecinek);
		System.out.println("Podany znak to: " + znak);
		
		//dlaczego to tak zadziałało?
		System.out.println("Wynik liczba1 + znak: " + (liczba1+znak));
	}
}
