
/*
 * Komentarze pozwalają na wstawienie opisu działania danego programu, funkcji czy funkcjonalności
 * naszego programu bez wpływu na szybkość (czy samo działanie) pisanego programu.
 * Obecnie pisany komentarz należy jest tzw. komentarzem wieloliniowym, tj. zaczyna się od 
 * slash(/)+gwiazdka(*) i na następnej gzwiazdce(*)+slach(/). Dodatkowe gwiazdki nic nie wnoszą
 * poza ewentalną estetyką komentarza (może ich nie być)
 */
/*
 
 	TAK JAK TUTAJ
 
 */
//to jest za to przykład komentarza jednoliniowego. Taki komentarz może pojawiać się w dowolnym
//miejscu jednak kończy się wraz z końem linii w pliku (tj.na ok. 8192 znaku)
/*
 * korzystanie z obu rodzajów komentarzy jest naprzemiennie dowolne (jak widać)
 */

//to jest początek każdego programu w języku Java. Ważne:
//- każdy program musi się zaczynać od słowa public class
//- nazwa następująca po tych słowach MUSI być taka sama jak nazwa pliku, w którym została zapisana
//WAŻNE! Po nazwie klasy MUSI wystąpić para nawiasów klamrowych {}. To w nich umieszczamy właściwy
//kod aplikacji!
public class Glowna {
	//jeżeli program ma się uruchamiać np. w konsoli nasza wcześniej zdefiniowana klasa MUSI posiadać
	//poniższą funkcję. MUSI ONA WYGLĄDAĆ DOKŁADNIE TAK (jak pierwsza linia kodu zaraz pod 
	//komentarzem). Funkcja również musi posiadać nawiasy klamrowe (ograniczające jej istnienie!)
	public static void main(String[] args) {
		//przykład wyświetlenia dowolnego tekstu w konsoli systemu bądz programu. Należy pamiętać,
		//że tekst MUSI być otoczony cudzysłowiem i być wpisany w nawiasy
		//po poleceniu MUSI wystąpić średnik (jest to odpowik kropki w zdaniu)
		System.out.print("WITAJ W JEZYKU JAVA!");
		
		//prócz print, do dyspozycji mamy także funkjcę append() (działa jak print z punktu widzenia
		//początkującego programisty) oraz println() (funkcja wypisuje tekst i przenosi znak
		//pisania do nowej linii). Przykłady działania:
		System.out.println("Linia numer 1!");
		System.out.println("Linia numer 2!");
		System.out.append("Linia numer 3!");
		System.out.println("Linia numer 4!");
		System.out.print("Linia numer 5!");
		System.out.print("Linia numer 6!");
		System.out.println(); //<- linia 7, pusta (tylko znak nowej linii)
		System.out.print("Linia numer 8!");
	}
}
